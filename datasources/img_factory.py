import matplotlib.pyplot as plt
import os
import time
import pytz
from datetime import datetime
import pandas as pd

from datasources.datasource import DatasourceFactory
from datasources.labels_factory import create_multilabels_from_meters
from datasources.paths_manager import IMGS
from constants.constants import *
from constants.enumerates import *
from utils.logger import TIMING, timing

def generate_heatmaps(datasource, building, device, start, end):

    dataset_name = datasource.get_name()
    window = 600
    path = _make_dirs(dataset_name, building, device)

    # daylight savings correction
    tz = datasource.dataset.metadata.get('timezone')
    if tz is None:
        raise RuntimeError("'timezone' is not set in dataset metadata.")
    
    if not _is_dst(start, tz) and _is_dst(end, tz):
        # missing data (1 hour) on start of summertime (start is before, end is during). have to add 1 day and later
        # remove excess data
        end = pd.Timestamp(end) + pd.Timedelta("1D")
        end = end.strftime(r"%Y-%m-%d")

    df, metergroup = datasource.read_selected_appliances([device], start=start, end=end,
        building=building, include_mains=True)
    
    datasource.clean_nans(df)

    # remove incomplete days (caused by daylight savings irregularities)
    cutoff = len(df) - (len(df) % window)
    df = df.iloc[:cutoff, :]

    # make date range for filenames
    datelist = pd.date_range(start=start, end=end, freq="H", closed="left").strftime(r"%Y_%m_%d_%H").to_list()

    # generate column name of mains
    mains = metergroup.mains()
    mains_col = mains.identifier

    # remove mains from metergoup as no labels are required for it
    metergroup.meters.remove(mains)

    appl_col = metergroup.meters[0].identifier

    # make labels from appliance data
    labels = create_multilabels_from_meters(df.drop(columns=[mains_col]), metergroup)

    # reshape data to daywise
    mains = df[mains_col].to_numpy().reshape((-1, window))
    appl = df[appl_col].to_numpy().reshape((-1, window))
    labels = labels.to_numpy().reshape((-1, window)).any(axis=1)
    # standardize data and make array of mains without appliance activations
    standard_mains = _standardize(mains)
    diff = _standardize((mains - appl).clip(min=0))

    vmin, vmax = standard_mains.min(), standard_mains.max()

    start_time = time.time() if TIMING else None
    
    for i in range(len(labels)):
        ts = datelist[i]
        fname = os.path.join(path, ts + r"_{}.png")

        # always make image from mains
        arr = standard_mains[i, :].reshape(60, 10, order="F")
        _make_img(arr=arr, vmin=vmin, vmax=vmax)

        if labels[i]:
            # appliance is active in that window: save image from mains as 1 and 
            # make image from mains w/o appliance, save as 0
            plt.savefig(fname.format(1), dpi=300)

            arr = diff[i, :].reshape(60, 10, order="F")
            _make_img(arr=arr, vmin=vmin, vmax=vmax)
            plt.savefig(fname.format(0), dpi=300)
        
        else:
            # save image from mains as 0
            plt.savefig(fname.format(0), dpi=300)

        plt.close("all")
    
    timing('Make images {}'.format(round(time.time() - start_time, 2)))

def _make_dirs(dataset_name, building, device):
    """Make the directory for the images: ../IMGS/dataset/building/device

    Args:
        dataset_name (str): name of dataset
        building (int): building number
        device (str): device name

    Returns:
        dir (str): path to directory for images
    """

    dir = os.path.join(IMGS, dataset_name, device, str(building))
    os.makedirs(dir, exist_ok=True)
    return dir

def _make_img(arr, vmin, vmax):
    """Make image from given array.

    Args:
        arr (np.ndarray): 2d array of power data
        vmin (float): min value of colormap range
        vmax (float): max value of colormap range

    Returns:
        fig (Figure): matplotlib figure of the image
    """

    fig, ax = plt.subplots(1)
    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    # make very small image, goal is 300x300px image which is achieved via dpi when saving the img
    fig.set_size_inches(1, 1)
    ax.axis('off')
    ax.imshow(arr, cmap="coolwarm", vmin=vmin, vmax=vmax, aspect="auto")

    return fig

def _standardize(arr):
    """Standardize the given array

    Args:
        arr (np.ndarray): any ndarray

    Returns:
        standard_arr (np.ndarray): array with mean of 0 and std of 1
    """
    
    mean = arr.mean()
    std = arr.std()

    standard_arr = (arr - mean) / std

    return standard_arr

def _is_dst(dt, tz):
    tz = pytz.timezone(tz)
    dt = datetime.strptime(dt, r"%Y-%m-%d")
    dt = tz.localize(dt)
    return dt.tzinfo._dst.seconds != 0

def img_as_plot(datasource, building: int, device: str, day: str, path: str=None):

    dataset_name = datasource.get_name()

    end = pd.Timestamp(day) + pd.Timedelta("1D")
    end = end.strftime(r"%Y-%m-%d")

    df, metergroup = datasource.read_selected_appliances([device], start=day, end=end,
        building=building, include_mains=True)
    
    datasource.clean_nans(df)

    # generate column name of mains
    mains = metergroup.mains()
    mains_col = mains.identifier

    # remove mains from metergoup as no labels are required for it
    metergroup.meters.remove(mains)

    appl_col = metergroup.meters[0].identifier

    # make labels from appliance data
    labels = create_multilabels_from_meters(df.drop(columns=[mains_col]), metergroup)

    # reshape data to daywise
    mains = df[mains_col].to_numpy()
    appl = df[appl_col].to_numpy()
    labels = labels.to_numpy().any(axis=1)
    # standardize data and make array of mains without appliance activations
    standard_mains = _standardize(mains)
    diff = _standardize((mains - appl).clip(min=0))

    vmin, vmax = standard_mains.min(), standard_mains.max()

    arr1 = standard_mains.reshape(600, 24, order="F")
    arr2 = diff.reshape(600, 24, order="F")

    for arr, state in zip([arr1, arr2], ["on", "off"]):
        fig, ax = plt.subplots(1)
        #fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
        # make very small image, goal is 300x300px image which is achieved via dpi when saving the img
        #fig.set_size_inches(1, 1)
        im = ax.imshow(arr, cmap="coolwarm", vmin=vmin, vmax=vmax, aspect="auto")
        ax.set_xlabel("Hours")
        ax.set_ylabel("Seconds x 6")
        ax.set_title("{} : {}, {} {}".format(dataset_name, day, device, state))

        plt.colorbar(im)
        plt.show()
        
        if path:
            fname = "heatmap_{}_{}.svg".format(day, state)
            plt.savefig(os.path.join(path, fname))