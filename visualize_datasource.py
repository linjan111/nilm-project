# script for visualizing the power values of datasets in a specific timeframe, for chosen
# devices. Was used for checking inconsistencies in datasets, where single device load > compound load

from datasources.datasource import DatasourceFactory
from datasources.img_factory import img_as_plot
import plotly.graph_objects as go
import plotly.subplots

appliance = ["washing machine"]
dataset = "ukdale"
start="2013-04-17"
end="2013-04-18"
building = 1

if dataset == "refit":
    datasource = DatasourceFactory.create_refit_datasource()
elif dataset == "ukdale":
    datasource = DatasourceFactory.create_uk_dale_datasource()

some_data = datasource.read_selected_appliances(appliance, start=start, end=end,
building=building, include_mains=True)

df = some_data[0]
df.columns = ["appl", "mains"]
indexer = df["appl"] > df["mains"]
df["appl"][indexer] = df["mains"][indexer]

names = ["Washing machine", "Mains"]
fig = plotly.subplots.make_subplots(rows=2, subplot_titles=names, shared_xaxes=True, vertical_spacing=0.1)

row = 1
for col, name in zip(df.columns, names):
    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df[col],
            line=go.scatter.Line(),
            #name = name
        ),
        row = row,
        col = 1
    )
    fig.update_yaxes(title_text="Power [W]", row=row, col=1)
    row +=1
fig.update_yaxes(range=[0, 4500])
fig.update_layout(showlegend=False)
fig.show()
fig.write_image("thesis_plots/data_of_heatmap.svg")

#img_as_plot(datasource, building, appliance[0], start, path="thesis_plots")
# %%
