# main file for running torchnilm experiments
import torch
from lab.nilm_experiments import *
from constants.constants import *
from constants.enumerates import *

torch.set_float32_matmul_precision("medium")

experiment_parameters = {
    EPOCHS: 100,
    ITERATIONS: 1,
    INFERENCE_CPU: False,
    SAMPLE_PERIOD: 6,
    BATCH_SIZE: 4096,
    PREPROCESSING_METHOD: SupportedPreprocessingMethods.SEQ_T0_SEQ,
    FIXED_WINDOW: 600,
    FILLNA_METHOD: SupportedFillingMethods.FILL_INTERPOLATION,
    TRAIN_TEST_SPLIT: 0.8,
    NOISE_FACTOR: 0.1,
}

devices = [
    ElectricalAppliances.KETTLE,
    # ElectricalAppliances.MICROWAVE,
    # ElectricalAppliances.FRIDGE,
]


experiment_categories= [
    # SupportedExperimentCategories.MULTI_CATEGORY,
    SupportedExperimentCategories.SINGLE_CATEGORY
]

model_hparams = ModelHyperModelParameters(
    [
        {
            "model_name": "BasicModel_v3_4",
            "hparams": {"input_len": experiment_parameters[FIXED_WINDOW], "preprocess": experiment_parameters[PREPROCESSING_METHOD]}
        }
    ]
)
experiment = NILMExperiments(project_name="Exp3",
                            clean_project=False,
                            devices=devices,
                            save_timeseries_results=False,
                            experiment_categories=experiment_categories,
                            experiment_volume=SupportedExperimentVolumes.SMALL_VOLUME,
                            experiment_parameters=ExperimentParameters(**experiment_parameters),
                            save_model=True,
                            export_plots=False,
                            progress_bar=False,
                            precision=16
                            )

experiment.run_benchmark(model_hparams=model_hparams)
