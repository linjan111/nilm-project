import os
import pandas as pd
import logging
import pdb


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def merge_reports(merging_dicts, target_dir, report_path="benchmark/results/statistical_report.csv"):
    """Merge statistical reports from different projects

    Args:
        merging_dicts (list): a list of dicts. Each dict needs a field with key "project_name". Optionally,
        a dict for renaming models and a list of models to include in the merged report can be
        specified.
        example_list = [
            {
                "project_name": "Exp1",
                "rename": {"BasicModel_v3": "Basic_3"},
                "models": ["BasicModel_v3"]
            },
            {
                "project_name": "Exp2",
            }
        ]    
        target_dir (str): where to save the merged stat report
        report_path (str, optional): where in the projects the stat reports lie. Defaults to "benchmark/results/statistical_report.csv".
    """


    dfs = []

    for proj_dict in merging_dicts:
        path = os.path.join("../output", proj_dict["project_name"], report_path)
        df = pd.read_csv(path, sep=",")

        if "models" in proj_dict.keys():
            df = df[df["model"].isin(proj_dict["models"])]        

        if "rename" in proj_dict.keys():
            df["model"] = df["model"].replace(proj_dict["rename"])

        dfs.append(df)

    df = pd.concat(dfs, axis=0, ignore_index=True, sort=False)
    
    os.makedirs(target_dir, exist_ok=True)
    target_path = os.path.join(target_dir, "merged_stat_report.csv")
    df.to_csv(target_path)

    logging.info("Saved merged statistical report to {}.".format(target_path))



if __name__ == "__main__":

    ex_dicts = [
        # {
        #     "project_name": "Exp1RollingWindow",
        #     "rename": {"BasicModel_v3_5": "Basic_1b_32 bit"},
        #     "models": ["BasicModel_v3_5"]
        # },
        # {
        #     "project_name": "Exp2",
        #     "rename": {"FCN": "FCN_32 bit", "ResNet1D": "ResNet1D_32 bit", "S2P": "S2P_32 bit"}
        # },
        # {
        #     "project_name": "Exp3",
        #     "rename": {"FCN": "FCN_16 bit", "ResNet1D": "ResNet1D_16 bit", "S2P": "S2P_16 bit", "BasicModel_v3_5": "Basic_1b_16 bit"}
        # }
        {
            "project_name": "Exp1Seq2Seq",
            "rename": {"BasicModel_v3_4": "Basic_1a_32 bit"},
            "models": ["BasicModel_v3_4"]
        },
        {
            "project_name": "Exp2",
            "rename": {"FCN": "FCN_32 bit", "S2P": "S2P_32 bit", "ResNet1D": "ResNet1D_32 bit"}
        },
        {
            "project_name": "Exp3",
            "rename": {"FCN": "FCN_16 bit", "S2P": "S2P_16 bit", "ResNet1D": "ResNet1D_16 bit", "BasicModel_v3_4": "Basic_1a_16 bit"},
            "models": ["BasicModel_v3_4", "FCN", "S2P", "ResNet1D"]
        }
        ]       

    merge_reports(
        ex_dicts,
        target_dir="../output/Exp3/benchmark/results")