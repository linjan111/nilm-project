import warnings
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import plotly.subplots
import os
import numpy as np

from constants.constants import *
from constants.enumerates import StatMeasures
from utils.helpers import list_intersection, experiment_name_format



pd.set_option('mode.chained_assignment', None)


def plot_radar_chart(data: pd.DataFrame = None, num_columns: list = None, plot_title: str = None,
                     save_name: str = None, ):
    if data.empty:
        warnings.warn('Empty dataframe')
    else:
        fig = go.Figure()
        models = data[COLUMN_MODEL].unique()
        # experiments = data[COLUMN_EXPERIMENT]

        for col in num_columns:
            if COLUMN_MAE in col:
                data.loc[:, col] = 1 - data[col] / data[col].max()
            if COLUMN_RETE in col:
                data.loc[:, col] = 1 - data[col]

        for model in models:
            r = data[data[COLUMN_MODEL] == model][num_columns].values[0].tolist()
            fig.add_trace(go.Scatterpolar(
                r=r,
                theta=[n.upper() for n in num_columns],
                fill='toself',
                name=model
            ))

        # for experiment in experiments:
        #     r = data[data[COLUMN_EXPERIMENT] == experiment][num_columns].values[0].tolist()
        #     ex = experiment.split("_")[5:]
        #     fig.add_trace(go.Scatterpolar(
        #         r=r,
        #         theta=[n.split("_")[0].upper() for n in num_columns],
        #         fill='toself',
        #         name="{} house {}".format(ex[1], ex[0])
        #     ))

        fig.update_layout(
            polar=dict(
                radialaxis=dict(
                    visible=True,
                    range=[0, 1]
                )),
            showlegend=True,
            title=plot_title
        )

        fig.write_image(save_name)


def plot_dataframe(data: pd.DataFrame = None, metrics: list = None, measures: list = None, appliances: list = None,
                   categories: list = None, experiments: list = None, plot_bar: bool = True, plot_spider: bool = True,
                   plots_save_path: str = None, facet_col: str = None, mean_tests: bool = False):


    if metrics is None:
        metrics = [COLUMN_F1, COLUMN_MAE, COLUMN_RETE, COLUMN_RECALL, COLUMN_PRECISION, COLUMN_ACCURACY, COLUMN_EPOCH_TIME]
        # metrics = [COLUMN_F1, COLUMN_RETE, COLUMN_RECALL, COLUMN_PRECISION, COLUMN_ACCURACY]
    if measures is None:
        measures = [StatMeasures.MEAN]

    categories = list_intersection(data[COLUMN_CATEGORY].unique(), categories)
    experiments = list_intersection(data[COLUMN_EXPERIMENT].unique(), experiments)

    num_cols = ['_'.join([metric, measure.value]) for metric in metrics for measure in measures]
    num_cols = list_intersection(num_cols, data.columns.tolist())

    cat_cols = [COLUMN_MODEL, COLUMN_MODEL_VERSION, COLUMN_CATEGORY, COLUMN_APPLIANCE, COLUMN_EXPERIMENT]
    if facet_col:
        cat_cols.append(facet_col)
    cat_cols = list_intersection(cat_cols, data.columns.tolist())

    num_cols.sort()
    cat_cols.sort()

    data = data[cat_cols + num_cols]
    data = data[(data[COLUMN_CATEGORY].isin(categories)) & (data[COLUMN_EXPERIMENT].isin(experiments))]

    if COLUMN_MODEL_VERSION in cat_cols:
        data[COLUMN_MODEL] = data[COLUMN_MODEL] + ' ' + data[COLUMN_MODEL_VERSION]

    if not appliances:
        appliances = data[COLUMN_APPLIANCE].unique()

    appliances = [app.title() for app in appliances]
    for appliance in appliances:
        for category in categories:
            temp = data[data[COLUMN_CATEGORY] == category]
            if plot_bar:
                for num_col in num_cols:
                    metric = num_col.split('_')[0].upper() + '(' + num_col.split('_')[-1].lower() + ')'
                    title = '{}_{}'.format(appliance, metric)
                    to_plot = temp[cat_cols + [num_col]]
                    to_plot.loc[:, COLUMN_EXPERIMENT] = to_plot[COLUMN_EXPERIMENT].apply(experiment_name_format)

                    if COLUMN_EPOCH_TIME in num_col:
                        to_plot = to_plot.drop_duplicates(subset=num_col)
                        fig = px.bar(
                            to_plot, x=COLUMN_MODEL, y=num_col,
                            facet_col=facet_col, title=None, height=400,
                            labels={
                                num_col: "train time / epoch [s]"
                            }
                        )
                        
                    else:

                        if mean_tests:
                            # show only two categories for single test results: same building as train and mean of results from different buildings than train
                            to_plot[COLUMN_EXPERIMENT] = to_plot[COLUMN_EXPERIMENT].apply(
                                lambda x: "Same as train" if "UKDALE 1" in x else "Others (Mean)"
                            )
                            if facet_col:
                                groups = [COLUMN_EXPERIMENT, COLUMN_MODEL, facet_col]
                            else:
                                groups = [COLUMN_EXPERIMENT, COLUMN_MODEL]
                            grouped = to_plot.groupby(groups)
                            to_plot = grouped.aggregate(np.mean).reset_index()

                        fig = px.bar(to_plot, x=COLUMN_EXPERIMENT, y=num_col,
                                    color=COLUMN_MODEL, barmode='group',
                                    facet_col=facet_col, title=None, height=400,
                                    labels={
                                        COLUMN_EXPERIMENT: "test building",
                                        num_col: num_col.rpartition("_")[0],
                                        },
                                    text=num_col
                                    )
                        
                    if mean_tests:
                        fig.update_layout(xaxis={"categoryorder": "array", "categoryarray": ["Same as train", "Others (Mean)"]})
                    save_name = plots_save_path + PLOT_BAR + ' ' + title + PNG_EXTENSION
                    fig.write_image(save_name)

            if plot_spider:
                # for experiment in temp[COLUMN_EXPERIMENT].unique():
                #     title = '{}: Comparison for experiment {} ({})'.format(appliance, experiment_name_format(experiment),
                #                                                            category.lower())
                #     save_name = plots_save_path + PLOT_SPIDER + ' ' + title + PNG_EXTENSION
                #     plot_radar_chart(data=temp[temp[COLUMN_EXPERIMENT] == experiment], num_columns=num_cols,
                #                      plot_title=title, save_name=save_name)

                title = '{}: Comparison for category {} '.format(appliance, category.lower())
                save_name = plots_save_path + PLOT_SPIDER + ' ' + title + ".svg"
                plot_radar_chart(data=temp, num_columns=num_cols,
                                    plot_title=title, save_name=save_name)



def plot_results_from_report(appliances: list = None, report_file_path: str = None, metrics: list = None,
                             measures: list = None, categories: list = None, experiments: list = None,
                             plots_save_path: str = None):

    if measures is None:
        measures = [StatMeasures.MEAN]
    if metrics is None:
        metrics = [COLUMN_F1, COLUMN_MAE, COLUMN_RETE, COLUMN_RECALL, COLUMN_PRECISION, COLUMN_ACCURACY]
    if appliances:
        appliances = [app.upper() for app in appliances]
    if report_file_path:
        xl = pd.ExcelFile(report_file_path, engine=OPENPYXL)
        sheets = xl.sheet_names
        sheets = list_intersection(sheets, appliances)
        for sheet in sheets:
            print('#' * 40)
            print(sheet)
            print('#' * 40)
            data = xl.parse(sheet)
            plot_dataframe(data, metrics, measures, appliances, categories, experiments, plots_save_path=plots_save_path)

def plot_timeseries(timeseries_dict: dict, folder: str = None, n_points: int = 100):
    """Plot inference timeseries results, showing groundtruth and prediction in a line plot.

    Several results (different devices or experiments) can be shown in multiple subplots.

    Args:
        timeseries_dict (dict): nested dictionary describing how to plot each timeseries, e.g.

        timeseries_dict = {"ts_1": {"path": "path/to/file", "start": 100, "end": 350}}.
        
        start and end indicate which range of values from the timeseries should be visualized.
        If end is not specified, <n_points> values beyond start are plotted by default.

        folder (str): where to save the plot. If not specified, only shows the plot and doesn't save.
        n_points (int): if no "end" is specified for a timeseries, it is set n_points after start.
    """

    devices = [os.path.normpath(timeseries_dict[key]["path"]).split(os.sep)[-5] for key in timeseries_dict]
    fig = plotly.subplots.make_subplots(
        cols=len(timeseries_dict), rows=2, subplot_titles=devices, vertical_spacing=0.05, shared_xaxes=True, horizontal_spacing=0.1,
        x_title="Seconds x 6", y_title="Power [W]"
        
        )
    filename = "subplots"

    plot_specs = {
        "ground": {"name":"Appliance", "color":'#ff7f0e', "showlegend": True},
        "preds": {"name": "Prediction", "color": '#1f77b4', "showlegend": True},
        "mains": {"name": "Mains", "color": '#2ca02c', "showlegend": True}
    }
    col = 1
    for ts in timeseries_dict:

        path = timeseries_dict[ts]["path"]
        start_idx = timeseries_dict[ts]["start"]
        try:
            end = timeseries_dict[ts]["end"]
        except KeyError:
            # "end" not specified, use default n_points
            end = start_idx + n_points
        
        df = pd.read_csv(path)
        df = df.iloc[start_idx:end, :].reset_index()

        x = df.index
        plots = [["mains", "ground"], ["ground", "preds"]]
        row = 1
        for plot in plots:
            for key in plot:

                fig.add_trace(
                    go.Scatter(
                        x=x,
                        y=df[key],
                        line=go.scatter.Line(color=plot_specs[key]["color"]),
                        showlegend = plot_specs[key]["showlegend"],
                        name = plot_specs[key]["name"]
                    ),
                    row = row,
                    col = col,
                )
                plot_specs[key]["showlegend"] = False
                
            row +=1
        
        col +=1
        filename += "_" + ts
        
    fig.update_layout(title="sometitle", showlegend=True, autosize=False,
    width=800,
    height=400)

    if folder:
        path = os.path.join(folder, filename + ".svg")
        fig.write_image(path)
        print("Plot saved to: {}".format(path))
    else:
        fig.show()

def plot_scatter(df: pd.DataFrame, x: str, y: str, symbol:str, save_name: str, plots_save_path: str, color: str=None):
    fig = px.scatter(data_frame=df, x=x, y=y, symbol=symbol, color=color)
    compress_legend(fig)
    fig.update_traces(marker={"size": 13})
    fig.write_image(os.path.join(plots_save_path, save_name + ".svg"))


def compress_legend(fig):
   group1_base, group2_base  = fig.data[0].name.split(",")
   lines_marker_name = []
   for i, trace in enumerate(fig.data):
       part1,part2 = trace.name.split(',')
       if part1 == group1_base:
           lines_marker_name.append({"line": trace.line.to_plotly_json(), "marker": trace.marker.to_plotly_json(), "mode": trace.mode, "name": part2.lstrip(" ")})
       if part2 != group2_base:
           trace['name'] = ''
           trace['showlegend']=False
       else:
           trace['name'] = part1
   
   ## Add the line/markers for the 2nd group
   for lmn in lines_marker_name:
       lmn["line"]["color"] = "black"
       lmn["marker"]["color"] = "black"
       fig.add_trace(go.Scatter(y=[None], **lmn))
   fig.update_layout(legend_title_text='', 
                     legend_itemclick=False,
                     legend_itemdoubleclick= False)