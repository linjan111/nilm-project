import warnings
import torch.nn as nn
import torch.nn.functional as F
import torch

class BasicModelBlock_v3(nn.Module):
    """1 Conv1D followed by batchnorm, adding bypass afterwards"""

    def __init__(self, in_channels, out_channels, kernel_size, padding):
        super().__init__()
        
        self.block = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size, padding=padding),
            nn.BatchNorm1d(out_channels),
        )
        self.bypass = nn.Conv1d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        y = self.block(x)
        y += self.bypass(x)
        y = nn.ReLU()(y)

        return y

class BasicModelBlock_v3_5(nn.Module):
    """1 depthwise separable conv1d followed by batchnorm, adding bypass afterwards"""

    def __init__(self, in_channels, out_channels, kernel_size, padding):
        super().__init__()
        
        self.block = nn.Sequential(
            ConvDepthSep(in_channels, out_channels, kernel_size, padding=padding),
            nn.BatchNorm1d(out_channels),
        )
        self.bypass = nn.Conv1d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        y = self.block(x)
        y += self.bypass(x)
        y = nn.ReLU()(y)

        return y

class BasicModelBlock_v5(nn.Module):
    """Uses the inverted residual block (MBconv), which also uses depthwise separable convs. Add 
    bypass afterwards."""


    def __init__(self, in_channels, out_channels, kernel_size, padding, expansion=4):
        super().__init__()
        
        # inverted residual block: narrow - wide - narrow, with depthwise separable convolution
        wide_channels = in_channels * expansion
        self.block = nn.Sequential(
            nn.Conv1d(in_channels, wide_channels, kernel_size=1),
            nn.ReLU(),
            nn.Conv1d(in_channels=wide_channels, 
                      out_channels=wide_channels,
                      kernel_size=kernel_size,
                      padding=padding,
                      groups=wide_channels),
            nn.ReLU(),
            nn.Conv1d(wide_channels, out_channels, kernel_size=1),
        )
        self.bypass = nn.Conv1d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        y = self.block(x)
        z = y + self.bypass(x)
        return z


class ConvDepthSep(nn.Module):
    """A module for performing 1D depthwise separable convolutions.
    """

    def __init__(self, in_channels, out_channels, kernel_size, dilation=1, padding=0):
        super().__init__()
        self.depthwise = nn.Conv1d(
            in_channels=in_channels,
            out_channels=in_channels,
            kernel_size=kernel_size,
            dilation=dilation,
            groups=in_channels,
            padding=padding
        )
        self.pointwise = nn.Conv1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=1
        )

    def forward(self, x):
        x = self.depthwise(x)
        x = self.pointwise(x)
        return x

class SqueezeExcitation(nn.Module):
    """
    This block is a 1D implementation of the Pytorch SqueezeExcitation block found at torchvision.ops.misc.

    This block implements the Squeeze-and-Excitation block from https://arxiv.org/abs/1709.01507 (see Fig. 1).
    Parameters ``activation``, and ``scale_activation`` correspond to ``delta`` and ``sigma`` in eq. 3.

    Args:
        input_channels (int): Number of channels in the input image
        squeeze_channels (int): Number of squeeze channels
        activation (Callable[..., torch.nn.Module], optional): ``delta`` activation. Default: ``torch.nn.ReLU``
        scale_activation (Callable[..., torch.nn.Module]): ``sigma`` activation. Default: ``torch.nn.Sigmoid``
    """

    def __init__(
        self,
        input_channels: int,
        squeeze_channels: int,
        activation = nn.ReLU,
        scale_activation = nn.Sigmoid,
    ) -> None:
        super().__init__()
        # _log_api_usage_once(self)
        self.avgpool = nn.AdaptiveAvgPool1d(1)
        self.fc1 = nn.Conv1d(input_channels, squeeze_channels, 1)
        self.fc2 = nn.Conv1d(squeeze_channels, input_channels, 1)
        self.activation = activation()
        self.scale_activation = scale_activation()

    def _scale(self, input):
        scale = self.avgpool(input)
        scale = self.fc1(scale)
        scale = self.activation(scale)
        scale = self.fc2(scale)
        return self.scale_activation(scale)

    def forward(self, input):
        scale = self._scale(input)
        return scale * input

class ConvPool(nn.Module):
    def __init__(self, in_channels, out_channels, conv_kernel, pool_kernel):
        super().__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=conv_kernel
                ),
            nn.MaxPool2d(kernel_size=pool_kernel)
        )

    def forward(self, x):
        return self.conv(x)

class LinearDropRelu(nn.Module):
    def __init__(self, in_features, out_features, dropout=0):
        super(LinearDropRelu, self).__init__()
        self.linear = nn.Sequential(
            nn.Linear(in_features, out_features),
            nn.Dropout(dropout),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.linear(x)


class ConvDropRelu(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, dropout=0, groups=1, relu=True):
        super(ConvDropRelu, self).__init__()

        left, right = kernel_size // 2, kernel_size // 2
        if kernel_size % 2 == 0:
            right -= 1
        padding = (left, right, 0, 0)

        if relu:
            self.conv = nn.Sequential(
                nn.ZeroPad2d(padding),
                nn.Conv1d(in_channels, out_channels, kernel_size, groups=groups),
                nn.Dropout(dropout),
                nn.ReLU(inplace=True),
            )
        else:
            self.conv = nn.Sequential(
                nn.ZeroPad2d(padding),
                nn.Conv1d(in_channels, out_channels, kernel_size, groups=groups),
                nn.Dropout(dropout),
            )

    def forward(self, x):
        return self.conv(x)


class ConvBatchRelu(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, groups=1, relu=True, batch_norm=True):
        super(ConvBatchRelu, self).__init__()

        left, right = kernel_size // 2, kernel_size // 2
        if kernel_size % 2 == 0:
            right -= 1
        padding = (left, right, 0, 0)

        modules = [nn.ZeroPad2d(padding),
                   nn.Conv1d(in_channels, out_channels, kernel_size, groups=groups)]
        if batch_norm:
            modules.append(nn.BatchNorm1d(out_channels))
        if relu:
            modules.append(nn.ReLU(inplace=True))
        self.conv = nn.Sequential(*modules)

    def forward(self, x):
        return self.conv(x)


class IBNNet(nn.Module):
    def __init__(self, input_channels, output_dim=64, kernel_size=3, inst_norm=True, residual=True, max_pool=True):
        """
        Inputs:
            input_channels - Dimensionality of the input (seq_len or window_size)
            output_dim - Dimensionality of the output
        """
        super().__init__()
        self.residual = residual
        self.max_pool = max_pool

        self.ibn = nn.Sequential(
            ConvBatchRelu(kernel_size=kernel_size, in_channels=input_channels, out_channels=64,
                          relu=True, batch_norm=True),
            ConvBatchRelu(kernel_size=kernel_size, in_channels=64, out_channels=64,
                          relu=True, batch_norm=True),
            ConvBatchRelu(kernel_size=kernel_size, in_channels=64, out_channels=256,
                          relu=False, batch_norm=True),
        )
        modules = []
        if inst_norm:
            modules.append(nn.InstanceNorm1d(output_dim))
        modules.append(nn.ReLU(inplace=True))
        self.out_layer = nn.Sequential(*modules)

        if self.max_pool:
            self.pool = nn.MaxPool1d(2)

    def forward(self, x):
        x = x
        ibn_out = self.ibn(x)
        if self.residual:
            x = x + ibn_out
        else:
            x = ibn_out

        out = self.out_layer(x)
        if self.max_pool:
            pool_out = self.pool(out)
            return out, pool_out
        else:
            return out, None


class VIBDecoder(nn.Module):
    def __init__(self, k, drop=0, output_dim=1):
        super().__init__()
        self.conv = ConvDropRelu(1, 3, kernel_size=5, dropout=drop)
        self.flatten = nn.Flatten()
        self.feedforward = nn.Sequential(
            LinearDropRelu(k * 3, 2 * k, drop),
            LinearDropRelu(2 * k, k, drop),
            nn.Linear(k, output_dim)
        )

    def forward(self, x):
        encoding = x.unsqueeze(1)
        decoding = self.conv(encoding).squeeze()
        decoding = self.flatten(decoding)
        return self.feedforward(decoding)


# for ResNet1D
class MyConv1dPadSame(nn.Module):
    """
    extend nn.Conv1d to support SAME padding
    """
    def __init__(self, in_channels, out_channels, kernel_size, stride, groups=1):
        super(MyConv1dPadSame, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.groups = groups
        self.conv = torch.nn.Conv1d(
            in_channels=self.in_channels, 
            out_channels=self.out_channels, 
            kernel_size=self.kernel_size, 
            stride=self.stride, 
            groups=self.groups)

    def forward(self, x):
        
        net = x
        
        # compute pad shape
        in_dim = net.shape[-1]
        out_dim = (in_dim + self.stride - 1) // self.stride
        p = max(0, (out_dim - 1) * self.stride + self.kernel_size - in_dim)
        pad_left = p // 2
        pad_right = p - pad_left
        net = F.pad(net, (pad_left, pad_right), "constant", 0)
        
        net = self.conv(net)

        return net
        
class MyMaxPool1dPadSame(nn.Module):
    """
    extend nn.MaxPool1d to support SAME padding
    """
    def __init__(self, kernel_size):
        super(MyMaxPool1dPadSame, self).__init__()
        self.kernel_size = kernel_size
        self.stride = 1
        self.max_pool = torch.nn.MaxPool1d(kernel_size=self.kernel_size)

    def forward(self, x):
        
        net = x
        
        # compute pad shape
        in_dim = net.shape[-1]
        out_dim = (in_dim + self.stride - 1) // self.stride
        p = max(0, (out_dim - 1) * self.stride + self.kernel_size - in_dim)
        pad_left = p // 2
        pad_right = p - pad_left
        net = F.pad(net, (pad_left, pad_right), "constant", 0)
        
        net = self.max_pool(net)
        
        return net
    
class BasicBlock(nn.Module):
    """
    ResNet Basic Block
    """
    def __init__(self, in_channels, out_channels, kernel_size, stride, groups, downsample, use_bn, use_do, is_first_block=False):
        super(BasicBlock, self).__init__()
        
        self.in_channels = in_channels
        self.kernel_size = kernel_size
        self.out_channels = out_channels
        self.stride = stride
        self.groups = groups
        self.downsample = downsample
        if self.downsample:
            self.stride = stride
        else:
            self.stride = 1
        self.is_first_block = is_first_block
        self.use_bn = use_bn
        self.use_do = use_do

        # the first conv
        self.bn1 = nn.BatchNorm1d(in_channels)
        self.relu1 = nn.ReLU()
        self.do1 = nn.Dropout(p=0.5)
        self.conv1 = MyConv1dPadSame(
            in_channels=in_channels, 
            out_channels=out_channels, 
            kernel_size=kernel_size, 
            stride=self.stride,
            groups=self.groups)

        # the second conv
        self.bn2 = nn.BatchNorm1d(out_channels)
        self.relu2 = nn.ReLU()
        self.do2 = nn.Dropout(p=0.5)
        self.conv2 = MyConv1dPadSame(
            in_channels=out_channels, 
            out_channels=out_channels, 
            kernel_size=kernel_size, 
            stride=1,
            groups=self.groups)
                
        self.max_pool = MyMaxPool1dPadSame(kernel_size=self.stride)

    def forward(self, x):
        
        identity = x
        
        # the first conv
        out = x
        if not self.is_first_block:
            if self.use_bn:
                out = self.bn1(out)
            out = self.relu1(out)
            if self.use_do:
                out = self.do1(out)
        out = self.conv1(out)
        
        # the second conv
        if self.use_bn:
            out = self.bn2(out)
        out = self.relu2(out)
        if self.use_do:
            out = self.do2(out)
        out = self.conv2(out)
        
        # if downsample, also downsample identity
        if self.downsample:
            identity = self.max_pool(identity)
            
        # if expand channel, also pad zeros to identity
        if self.out_channels != self.in_channels:
            identity = identity.transpose(-1,-2)
            ch1 = (self.out_channels-self.in_channels)//2
            ch2 = self.out_channels-self.in_channels-ch1
            identity = F.pad(identity, (ch1, ch2), "constant", 0)
            identity = identity.transpose(-1,-2)
        
        # shortcut
        out += identity

        return out
