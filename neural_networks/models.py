import math
import torch
import torch.nn as nn
from torchnlp.nn.attention import Attention

from neural_networks.base_models import BaseModel
from neural_networks.custom_modules import ConvDropRelu, LinearDropRelu, ConvPool, ConvDepthSep, BasicModelBlock_v3, BasicModelBlock_v3_5, BasicModelBlock_v5, BasicBlock, MyConv1dPadSame
from constants.enumerates import SupportedPreprocessingMethods

class GELU(nn.Module):
    def forward(self, x):
        return 0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))


class PositionalEmbedding(nn.Module):
    def __init__(self, max_len, d_model):
        super().__init__()
        self.pe = nn.Embedding(max_len, d_model)

    def forward(self, x):
        batch_size = x.size(0)
        return self.pe.weight.unsqueeze(0).repeat(batch_size, 1, 1)


class PositionwiseFeedForward(nn.Module):
    def __init__(self, d_model, d_ff):
        super(PositionwiseFeedForward, self).__init__()
        self.w_1 = nn.Linear(d_model, d_ff)
        self.w_2 = nn.Linear(d_ff, d_model)
        self.activation = GELU()

    def forward(self, x):
        return self.w_2(self.activation(self.w_1(x)))


class LayerNorm(nn.Module):
    def __init__(self, features, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.weight = nn.Parameter(torch.ones(features))
        self.bias = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.weight * (x - mean) / (std + self.eps) + self.bias


class FeedForward(nn.Module):
    def __init__(self, dim, hidden_factor, dropout=0.):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(dim, hidden_factor),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(hidden_factor, dim),
            nn.Dropout(dropout)
        )

    def forward(self, x):
        return self.net(x)


class Seq2Point(BaseModel):

    def __init__(self, window_size, dropout=0, lr=None, output_dim: int = 1):
        super(Seq2Point, self).__init__()
        self.MODEL_NAME = 'Sequence2Point model'
        self.drop = dropout
        self.lr = lr

        self.dense_input = 50 * window_size  # 50 is the out_features of last CNN1

        self.conv = nn.Sequential(
            ConvDropRelu(1, 30, kernel_size=10, dropout=self.drop, groups=1),
            ConvDropRelu(30, 40, kernel_size=8, dropout=self.drop),
            ConvDropRelu(40, 50, kernel_size=6, dropout=self.drop),
            ConvDropRelu(50, 50, kernel_size=5, dropout=self.drop),
            ConvDropRelu(50, 50, kernel_size=5, dropout=self.drop),
            nn.Flatten()
        )
        self.dense = LinearDropRelu(self.dense_input, 1024, self.drop)
        self.output = nn.Linear(1024, output_dim)

    def forward(self, x):
        # x must be in shape [batch_size, 1, window_size]
        # eg: [1024, 1, 50]
        x = x.unsqueeze(1)
        x = self.conv(x)
        x = self.dense(x)
        out = self.output(x)
        return out


class WGRU(BaseModel):

    def __init__(self, dropout=0, lr=None, output_dim: int = 1):
        super(WGRU, self).__init__()

        self.drop = dropout
        self.lr = lr

        self.conv1 = ConvDropRelu(1, 16, kernel_size=4, dropout=self.drop)

        self.b1 = nn.GRU(16, 64, batch_first=True,
                         bidirectional=True,
                         dropout=self.drop)
        self.b2 = nn.GRU(128, 256, batch_first=True,
                         bidirectional=True,
                         dropout=self.drop)

        self.dense1 = LinearDropRelu(512, 128, self.drop)
        self.dense2 = LinearDropRelu(128, 64, self.drop)
        self.output = nn.Linear(64, output_dim)

    def forward(self, x):
        # x must be in shape [batch_size, 1, window_size]
        # eg: [1024, 1, 50]
        x = x.unsqueeze(1)
        x = self.conv1(x)
        # x (aka output of conv1) shape is [batch_size, out_channels=16, window_size-kernel+1]
        # x must be in shape [batch_size, seq_len, input_size=output_size of prev layer]
        # so we have to change the order of the dimensions
        x = x.permute(0, 2, 1)
        x = self.b1(x)[0]
        x = self.b2(x)[0]
        # we took only the first part of the tuple: output, h = gru(x)

        # Next we have to take only the last hidden state of the last b2gru
        # equivalent of return_sequences=False
        x = x[:, -1, :]
        x = self.dense1(x)
        x = self.dense2(x)
        out = self.output(x)
        return out


class SAED(BaseModel):

    def __init__(self, window_size, mode='dot', hidden_dim=16, num_heads=1, dropout=0, bidirectional=True, lr=None,
                 output_dim: int = 1):
        super(SAED, self).__init__()

        '''
        mode(str): 'dot' or 'general'--> additive
            default is 'dot' (additive attention not supported yet)
        ***in order for the mhattention to work, embed_dim should be dividable
        to num_heads (embed_dim is the hidden dimension inside mhattention
        '''
        self.num_heads = num_heads
        if num_heads > hidden_dim:
            num_heads = 1
            print('WARNING num_heads > embed_dim so it is set equal to 1')
        else:
            while hidden_dim % num_heads:
                if num_heads > 1:
                    num_heads -= 1
                else:
                    num_heads += 1

        self.drop = dropout
        self.lr = lr
        self.mode = 'dot'

        self.conv = ConvDropRelu(1, hidden_dim,
                                 kernel_size=4,
                                 dropout=self.drop)
        if num_heads>1:
            self.attention = nn.MultiheadAttention(embed_dim=hidden_dim,
                                                        num_heads=num_heads,
                                                        dropout=self.drop)
        else:
            self.attention = Attention(window_size, attention_type=mode)

        self.bgru = nn.GRU(hidden_dim, 64,
                           batch_first=True,
                           bidirectional=bidirectional,
                           dropout=self.drop)
        if bidirectional:
            self.dense = LinearDropRelu(128, 64, self.drop)
            self.output = nn.Linear(64, output_dim)
        else:
            self.dense = LinearDropRelu(64, 32, self.drop)
            self.output = nn.Linear(32, output_dim)

    def forward(self, x):
        # x must be in shape [batch_size, 1, window_size]
        x = x
        x = x.unsqueeze(1)
        x = self.conv(x)

        if self.num_heads>1:
            # x (aka output of conv1) shape is [batch_size, out_channels=16, window_size-kernel+1]
            # x must be in shape [batch_size, seq_len, input_size=output_size of prev layer]
            # so we have to change the order of the dimensions
            x = x.permute(0, 2, 1)
            x, _ = self.attention(query=x, key=x, value=x)
        else:
            x, _ = self.attention(x, x)
            x = x.permute(0, 2, 1)

        x = self.bgru(x)[0]
        # we took only the first part of the tuple: output, h = gru(x)
        # Next we have to take only the last hidden state of the last b1gru
        # equivalent of return_sequences=False
        x = x[:, -1, :]
        x = self.dense(x)
        out = self.output(x)
        return out


class SimpleGru(BaseModel):

    def __init__(self, hidden_dim=16, dropout=0, bidirectional=True, lr=None, output_dim=1):
        super(SimpleGru, self).__init__()

        '''
        mode(str): 'dot' or 'add'
            default is 'dot' (additive attention not supported yet)
        ***in order for the mhattention to work, embed_dim should be dividable
        to num_heads (embed_dim is the hidden dimension inside mhattention
        '''

        self.drop = dropout
        self.lr = lr

        self.conv = ConvDropRelu(1, hidden_dim,
                                 kernel_size=4,
                                 dropout=self.drop)

        self.bgru = nn.GRU(hidden_dim, 64,
                           batch_first=True,
                           bidirectional=bidirectional,
                           dropout=self.drop)
        if bidirectional:
            self.dense = LinearDropRelu(128, 64, self.drop)
            self.output = nn.Linear(64, output_dim)
        else:
            self.dense = LinearDropRelu(64, 32, self.drop)
            self.output = nn.Linear(32, output_dim)

    def forward(self, x):
        # x must be in shape [batch_size, 1, window_size]
        # eg: [1024, 1, 50]
        x = x
        x = x.unsqueeze(1)
        x = self.conv(x)
        x = x.permute(0, 2, 1)
        x = self.bgru(x)[0]
        x = x[:, -1, :]
        x = self.dense(x)
        out = self.output(x)
        return out


class DAE(BaseModel):
    def __init__(self, input_dim, dropout=0.2, output_dim=1):
        super().__init__()
        self.encoder = nn.Sequential(
            ConvDropRelu(1, 8, kernel_size=4, relu=False),
            nn.Flatten(),
            nn.Dropout(dropout),
            nn.Linear(input_dim * 8, input_dim * 8),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Linear(input_dim * 8, input_dim // 2),
            nn.ReLU(),
            nn.Dropout(dropout),
        )
        self.decoder = nn.Sequential(
            nn.Linear(input_dim // 2, input_dim * 8),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Unflatten(1, (8, input_dim)),
            nn.ConvTranspose1d(in_channels=8, out_channels=1, kernel_size=4, padding=3, stride=1, dilation=2),
            nn.Linear(input_dim, output_dim)
        )

    def forward(self, x):
        x = x
        # x must be in shape [batch_size, 1, window_size]
        # eg: [1024, 1, 50]
        x = x
        x = x.unsqueeze(1)
        x = self.encoder(x)
        x = self.decoder(x)
        return x


class FourierBLock(nn.Module):

    def __init__(self, input_dim, hidden_dim, dropout=0.0, mode='fft', leaky_relu=False):
        """
        Input arguments:
            input_dim - Dimensionality of the input (seq_len)
            hidden_dim - Dimensionality of the hidden layer in the MLP
            dropout - Dropout probability to use in the dropout layers
            mode - The type of mechanism inside the block. Currently, three types are supported; 'fft' for fourier,
            'att' for dot attention and 'plain' for simple concatenation.
                default value: 'fft'
            leaky_relu - A flag that controls whether leaky relu should be applied on the linear layer after the
            fourier mechanism.
                default value: False
        """
        super().__init__()
        self.mode = mode
        if self.mode == 'att':
            self.attention = Attention(input_dim, attention_type='dot')

        if leaky_relu:
            self.linear_fftout = nn.Sequential(
                nn.Linear(2 * input_dim, input_dim),
                nn.LeakyReLU(inplace=True),
            )
        else:
            self.linear_fftout = nn.Sequential(
                nn.Linear(2 * input_dim, input_dim),
            )

        self.linear_net = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(hidden_dim, input_dim)
        )

        self.norm1 = nn.LayerNorm(input_dim)
        self.norm2 = nn.LayerNorm(input_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):

        fft_out = self.norm1(x)
        if self.mode == 'fft':
            fft_out = torch.fft.fft(fft_out, dim=-1)
            fft_out = torch.cat((fft_out.real, fft_out.imag), dim=-1)
        elif self.mode == 'att':
            fft_out, _ = self.attention(fft_out, fft_out)
            fft_out = torch.cat((fft_out, fft_out), dim=-1)
        elif self.mode == 'plain':
            fft_out = torch.cat((fft_out, fft_out), dim=-1)

        fft_out = self.linear_fftout(fft_out)
        x = x + self.dropout(fft_out)
        x = self.norm2(x)
        linear_out = self.linear_net(x)
        x = x + self.dropout(linear_out)
        return x


class NFED(BaseModel):
    def __init__(self, depth, kernel_size, cnn_dim, output_dim: int = 1, **block_args):
        """
        Input arguments:
            depth - The number of fourier blocks in series
            kernel_size - The kernel size of the first CNN layer
            cnn_dim - Dimensionality of the output of the first CNN layer
        """
        super(NFED, self).__init__()
        self.drop = block_args['dropout']
        self.input_dim = block_args['input_dim']
        self.dense_in = self.input_dim * cnn_dim // 2

        self.conv = ConvDropRelu(1, cnn_dim, kernel_size=kernel_size, dropout=self.drop)
        self.pool = nn.LPPool1d(norm_type=2, kernel_size=2, stride=2)

        self.fourier_layers = nn.ModuleList([FourierBLock(**block_args) for _ in range(depth)])

        self.flat = nn.Flatten()
        self.dense1 = LinearDropRelu(self.dense_in, cnn_dim, self.drop)
        self.dense2 = LinearDropRelu(cnn_dim, cnn_dim // 2, self.drop)

        self.output = nn.Linear(cnn_dim // 2, output_dim)

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self.conv(x)
        x = x.transpose(1, 2).contiguous()
        x = self.pool(x)
        x = x.transpose(1, 2).contiguous()
        for layer in self.fourier_layers:
            x = layer(x)
        x = self.flat(x)
        x = self.dense1(x)
        x = self.dense2(x)
        out = self.output(x)
        return out

class FCN(BaseModel):

    def __init__(self, dilation_layers=9, depthwise_separable=False, output_dim: int = 1):
        """The Fully Convolutional Model.

        Args:
            dilation_layers (int, optional): Number of dilation layers. Dilation begins at 1 and doubles every layer. Defaults to 9.
        """
        super().__init__()

        self.MODEL_NAME = "Fully convolutional network"

        # either use depthwise separable or regular dilated convolutions
        if depthwise_separable:
            conv_op = ConvDepthSep
        else:
            conv_op = nn.Conv1d

        self.dilatedConvStack = nn.ModuleList()

        kwargs = {"in_channels": 128, "out_channels": 128, "kernel_size": 3}
        for i in range(1, dilation_layers+1):
            kwargs["dilation"] = 2**i
            self.dilatedConvStack.append(nn.Sequential(
                conv_op(**kwargs),
                nn.ReLU(inplace=True)
                )
            )

        self.conv_front = nn.Sequential(
            nn.Conv1d(in_channels=1, out_channels=128, kernel_size=9),
            nn.ReLU(inplace=True)
        )
        self.conv_end = nn.Conv1d(in_channels=128, out_channels=1, kernel_size=1)


    def forward(self, x):

        x = x.unsqueeze(1)
        x = self.conv_front(x)
        for layer in self.dilatedConvStack:
            x = layer(x)
        x = self.conv_end(x)

        return x

class IMGNILM(BaseModel):

    def __init__(self, dropout: float=0.25, output_dim: int = 1):
        super().__init__()

        self.MODEL_NAME = "Image NILM"

        self.conv_pool1 = ConvPool(in_channels=3, out_channels=16, conv_kernel=3, pool_kernel=2)
        self.conv_pool2 = ConvPool(in_channels=16, out_channels=32, conv_kernel=3, pool_kernel=2)
        self.conv_pool3 = ConvPool(in_channels=32, out_channels=64, conv_kernel=3, pool_kernel=2)
        self.drop = nn.Dropout2d(p=dropout)

        self.batch_norm_relu = nn.Sequential(
            nn.BatchNorm2d(num_features=64),
            nn.ReLU(inplace=True)
        )
        self.flatten = nn.Sequential(
            nn.Flatten(),
            # nn.Softmax(dim=1)
        )
        # self.dense1 = nn.Sequential(
        #     nn.Linear(in_features=78400, out_features=512),
        #     # nn.Softmax(dim=1)
        # )
        self.dense2 = nn.Sequential(
            nn.Linear(in_features=78400, out_features=1),
            # can not use Softmax in last layer (would return only 1's)
            # nn.Linear(in_features=355216, out_features=1),
            nn.Sigmoid()
        )

    def supports_imgs(self) -> bool:
        return True

    def forward(self, x):
        x = self.conv_pool1(x)
        x = self.conv_pool2(x)
        x = self.conv_pool3(x)
        x = self.drop(x)
        
        x = self.batch_norm_relu(x)
        
        x = self.flatten(x)
        # x = self.dense1(x)
        x = self.dense2(x)

        return x

class SomeBasicModel_v3_4(BaseModel):
    """Has a total of 6 layers of convs with bypass and batch norm after each conv"""

    def __init__(self, input_len, output_dim: int = 1, preprocess=SupportedPreprocessingMethods.SEQ_T0_SEQ):
        super().__init__()

        self.MODEL_NAME = "Basic Model v3_4"

        self.conv1 = nn.Conv1d(1, 16, 3, padding="same")
        self.conv2 = BasicModelBlock_v3(16, 32, 3, "same")
        self.conv3 = BasicModelBlock_v3(32, 64, 3, "same")
        self.conv4 = BasicModelBlock_v3(64, 128, 3, "same")
        self.conv5 = BasicModelBlock_v3(128, 16, 3, "same")
        self.conv_end = nn.Conv1d(16, 1, 3, padding="same")

        if preprocess in [SupportedPreprocessingMethods.MIDPOINT_WINDOW, SupportedPreprocessingMethods.ROLLING_WINDOW]:
            self.end = nn.Linear(in_features=input_len, out_features=output_dim)
        else:
            self.end = nn.Identity()

    def forward(self, x):

        x = x.unsqueeze(1)
        y1 = self.conv1(x)
        y2 = self.conv2(y1)
        y3 = self.conv3(y2)
        y4 = self.conv4(y3)
        y5 = self.conv5(y4)
        # y6 = self.conv6(y5)
        # y7 = self.conv7(y6)

        y8 = self.conv_end(y5)
        y9 = self.end(y8)

        return y9
    
class SomeBasicModel_v3_5(BaseModel):
    """Has a total of 6 layers of depthwise separable convs, with bypass and batch norm afterwards"""

    def __init__(self, input_len, output_dim: int = 1, preprocess=SupportedPreprocessingMethods.SEQ_T0_SEQ):
        super().__init__()

        self.MODEL_NAME = "Basic Model v3_5"

        self.conv1 = nn.Conv1d(1, 16, 3, padding="same")
        self.conv2 = BasicModelBlock_v3_5(16, 32, 3, padding="same")
        self.conv3 = BasicModelBlock_v3_5(32, 64, 3, padding="same")
        self.conv4 = BasicModelBlock_v3_5(64, 128, 3, padding="same")
        self.conv5 = BasicModelBlock_v3_5(128, 16, 3, padding="same")
        self.conv_end = nn.Conv1d(16, 1, 3, padding="same")

        if preprocess in [SupportedPreprocessingMethods.MIDPOINT_WINDOW, SupportedPreprocessingMethods.ROLLING_WINDOW]:
            self.end = nn.Linear(in_features=input_len, out_features=output_dim)
        else:
            self.end = nn.Identity

    def forward(self, x):

        x = x.unsqueeze(1)
        y1 = self.conv1(x)
        y2 = self.conv2(y1)
        y3 = self.conv3(y2)
        y4 = self.conv4(y3)
        y5 = self.conv5(y4)
        y6 = self.conv_end(y5)
        y7 = self.end(y6)

        return y7

class SomeBasicModel_v5(BaseModel):
    "Has a total of 6 layers of convs, 4 of which implementing the inverted residual block"

    def __init__(self, input_len, output_dim: int = 1, preprocess=SupportedPreprocessingMethods.SEQ_T0_SEQ):
        super().__init__()

        self.MODEL_NAME = "Basic Model v3_5"

        self.conv1 = nn.Conv1d(1, 16, 3, padding="same")
        self.conv2 = BasicModelBlock_v5(16, 32, 3, padding="same")
        self.conv3 = BasicModelBlock_v5(32, 64, 3, padding="same")
        self.conv4 = BasicModelBlock_v5(64, 128, 3, padding="same")
        self.conv5 = BasicModelBlock_v5(128, 16, 3, padding="same")
        self.conv_end = nn.Conv1d(16, 1, 3, padding="same")

        if preprocess in [SupportedPreprocessingMethods.MIDPOINT_WINDOW, SupportedPreprocessingMethods.ROLLING_WINDOW]:
            self.end = nn.Linear(in_features=input_len, out_features=output_dim)
        else:
            self.end = nn.Identity()

    def forward(self, x):

        x = x.unsqueeze(1)
        y1 = self.conv1(x)
        y2 = self.conv2(y1)
        y3 = self.conv3(y2)
        y4 = self.conv4(y3)
        y5 = self.conv5(y4)
        y6 = self.conv_end(y5)
        y7 = self.end(y6)

        return y7
    
class ResNet1D(BaseModel):
    """
    resnet for 1-d signal data, pytorch version
    from: https://arxiv.org/abs/2008.04063
 
    Shenda Hong, Oct 2019

    Input:
        X: (n_samples, n_channel, n_length)
        Y: (n_samples)
        
    Output:
        out: (n_samples)
        
    Pararmetes:
        in_channels: dim of input, the same as n_channel
        base_filters: number of filters in the first several Conv layer, it will double at every 4 layers
        kernel_size: width of kernel
        stride: stride of kernel moving
        groups: set larget to 1 as ResNeXt
        n_block: number of blocks
        n_classes: number of classes
        
    """

    def __init__(self, in_channels, base_filters, kernel_size, stride, groups, n_block, n_classes, downsample_gap=2, increasefilter_gap=4, use_bn=True, use_do=True, verbose=False, output_dim=1):
        super(ResNet1D, self).__init__()
        
        self.verbose = verbose
        self.n_block = n_block
        self.kernel_size = kernel_size
        self.stride = stride
        self.groups = groups
        self.use_bn = use_bn
        self.use_do = use_do

        self.downsample_gap = downsample_gap # 2 for base model
        self.increasefilter_gap = increasefilter_gap # 4 for base model

        # first block
        self.first_block_conv = MyConv1dPadSame(in_channels=in_channels, out_channels=base_filters, kernel_size=self.kernel_size, stride=1)
        self.first_block_bn = nn.BatchNorm1d(base_filters)
        self.first_block_relu = nn.ReLU()
        out_channels = base_filters
                
        # residual blocks
        self.basicblock_list = nn.ModuleList()
        for i_block in range(self.n_block):
            # is_first_block
            if i_block == 0:
                is_first_block = True
            else:
                is_first_block = False
            # downsample at every self.downsample_gap blocks
            if i_block % self.downsample_gap == 1:
                downsample = True
            else:
                downsample = False
            # in_channels and out_channels
            if is_first_block:
                in_channels = base_filters
                out_channels = in_channels
            else:
                # increase filters at every self.increasefilter_gap blocks
                in_channels = int(base_filters*2**((i_block-1)//self.increasefilter_gap))
                if (i_block % self.increasefilter_gap == 0) and (i_block != 0):
                    out_channels = in_channels * 2
                else:
                    out_channels = in_channels
            
            tmp_block = BasicBlock(
                in_channels=in_channels, 
                out_channels=out_channels, 
                kernel_size=self.kernel_size, 
                stride = self.stride, 
                groups = self.groups, 
                downsample=downsample, 
                use_bn = self.use_bn, 
                use_do = self.use_do, 
                is_first_block=is_first_block)
            self.basicblock_list.append(tmp_block)

        # final prediction
        self.final_bn = nn.BatchNorm1d(out_channels)
        self.final_relu = nn.ReLU(inplace=True)
        # self.do = nn.Dropout(p=0.5)
        self.dense = nn.Linear(out_channels, n_classes)
        # self.softmax = nn.Softmax(dim=1)
        
    def forward(self, x):
        x = x.unsqueeze(1)
        out = x
        
        # first conv
        if self.verbose:
            print('input shape', out.shape)
        out = self.first_block_conv(out)
        if self.verbose:
            print('after first conv', out.shape)
        if self.use_bn:
            out = self.first_block_bn(out)
        out = self.first_block_relu(out)
        
        # residual blocks, every block has two conv
        for i_block in range(self.n_block):
            net = self.basicblock_list[i_block]
            if self.verbose:
                print('i_block: {0}, in_channels: {1}, out_channels: {2}, downsample: {3}'.format(i_block, net.in_channels, net.out_channels, net.downsample))
            out = net(out)
            if self.verbose:
                print(out.shape)

        # final prediction
        if self.use_bn:
            out = self.final_bn(out)
        out = self.final_relu(out)
        out = out.mean(-1)
        if self.verbose:
            print('final pooling', out.shape)
        # out = self.do(out)
        out = self.dense(out)
        if self.verbose:
            print('dense', out.shape)
        # out = self.softmax(out)
        if self.verbose:
            print('softmax', out.shape)
        
        return out    